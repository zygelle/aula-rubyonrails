class UsersController < ApplicationController
    before_action :impede_nao_logado, except: [:new, :create]
    before_action :impede_logado, only: [:new]

    def new
        @user = User.new
    end

    def create
        @user = User.new(user_params)
        if @user.save
            log_in @user
            redirect_to @user
        else
            render 'new'
        end
    end

    def index
    end

    def show
        @user=User.find(params[:id])
    end

    private

    def user_params
        params.require(:user).permit(:name, :email, :password, :password_confirmation)
    end

    def impede_nao_logado
        if !logged_in?
            redirect_to root_path
        end
    end

    def impede_logado
        if logged_in?
            redirect_to current_user
        end
    end
end
